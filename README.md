## ninja-dyndep-depfile-failure

This project shows that the dyndep + depfile behavior seen in CMake with GCC
and Clang is in `ninja` itself.

To reproduce:

- `ninja` to build the project
- `touch foo` to trigger a rescan

This then comes up with the error:

```
ninja: build stopped: multiple rules generate foo.dyn.
```
